.. JB on programming  documentation master file, created by
   sphinx-quickstart on Wed May  1 12:11:27 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to JB on programming 's documentation!
==============================================

Contents:

.. toctree::
   :maxdepth: 1
   :glob:

   posts/*

Comments:

* If you have any comments please send me an e-mail to ``jacekfoo@gmail.com``.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

