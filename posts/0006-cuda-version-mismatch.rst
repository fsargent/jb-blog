When your cuda installation stops working (on DEBIAN)
=====================================================

There can be many signs for this error:

``deviceQuery`` returns::

   cudaGetDeviceCount returned 38-> no CUDA-capable device is detected

or you get::

    no CUDA-capable device is detected

In my case it was version mismatch between installed ``nvidia`` kernel module
and ``libcuda1`` library.

.. note::

    Normally this module is installed via ``DKMS`` so kernel module version
    should match version of ``nvidia-kernel-dkms``, but this is not always
    the case. . .

To check version of installed kernell module::

    sudo modinfo nvidia-current | grep version

For now it should be ``319.xxx``. If it has version mismatch ``libcuda1``
you have source of your errors (yay!).

