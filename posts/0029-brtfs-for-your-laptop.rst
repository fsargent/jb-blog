Should you use BRTFS for your laptop?
=====================================

TL; DR; Probably not.

Recently I re-installed by Debian desktop, and enabled full disk
encryption, (and LVM for that matter). I was also toying with the
idea of using BTRFS, which has many features I always wanted
to have, including:

* Fast fs-level compression
* Optional, out of write path, deduplication.
* Very nice features including super-easy resizing
  (adding more GBs to your file system takes
  seconds)
* Copy on write semantics

BTRFS is stable, yet I believe is unusable for a most of people.
If you are not a linux nerd, don't try, if you are you might,
but remember:

* Do backups, you should always do backups, especially if you
  use encrypted filesystems on ssd drives.
* Do yourself a favour and buy an USB stick, and burn there
  a linux live-cd.
* BTRFS **will** suprise you.

Here are two nasty suprises I had.

BTRFS COW doesnt play well with some usage patterns
---------------------------------------------------

If you are a linux nerd, you probably have some virtual machines,
COW (copy on write) doesn't play with them. Well everywhere
where you have large files that are written to often, it doesn't
play well with COW.

COW can be disabled on directory level, do to this issue
`chattr +C /dir` command, this will disable COW for
everything under `/dir`. Keep in mind that it works
**only on empty files and directories**, turning off COW
on a file with data, has **undefined behaviour**, and
most often is bad. Turning off COW for directories with files
is safe, but existing files will have COW enabled.

In my case Virtualbox failed with very non-obvious errors,
before I disabled COW.

BTRFS needs garbage colection (or something similar)
----------------------------------------------------

Basically BTRFS kind-of lies to the OS when reporting free space.
You can have full filesystem and yet BTRFS will report 100GB
of free space. I don't try to understand what it is,
BTRFS wiki says things like: "The primary purpose of the balance
feature is to spread block groups accross all devices so they
match constraints defined by the respective profiles."

Usable free space on your disk can be seen using `btrfs fi show`
Which will display: total system size, and how much space
is currently used by btrfs.

In my case:

.. code-block:: bash

    # btrfs fi show

    Label: none  uuid:
        Total devices 1 FS bytes used 613.29GiB
        devid    1 size 745.06GiB used 649.06GiB path /dev/mapper/

I have 745GB partition, of which 649GB is used by btrfs,
however, only 613 is used for files.

This can be fixed by issuing something like::

    btrfs balance start -dusage=<magic number> / &

``<<magic number>`` is a percentage value, and BTRFS will try to
"rebalance" only chunks filled in less than this <<magic number>>,
the bigger number you put there the longer will it take, and the
more space you'll reclaim. You can start with percentage value
of used space you have on your drive.
