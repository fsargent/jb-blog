How to change what external program calibre uses to open files
==============================================================

Here are two (mine) related questions:

* http://stackoverflow.com/q/32256039/7918
* http://unix.stackexchange.com/q/225103/5612

Abbreviated solution is here:

Calibre uses ``QDesktopServices.openUrl(qurl)`` method, that uses
``xdg-utils`` under the hood to open files.

IMO ``xdg-utils`` is a stinking mess of fragile bash scripts, that ...
behaves erratically at best.

To set what program is used to launch a certain file you'll need to:

1. Find mimetype of this file, this can be done by:

  .. code-block:: bash

     xdg-mime query filetype <filename>

  This will print out mimetype of that file, for example ``application/pdf``.

2. Then you'll need to find a ``desktop`` file associated with application
   you want to use. In debian desktop files are in ``/usr/share/applications``.

   In case of pdf file I wanted to open it using ``evince`` that has
   ``desktop.file`` named ``evince.desktop``.

3. Associate desktop file with mimetype, using command:
   ``xdg-mime default evince.desktop application/pdf``.

4. Werify it works using ``xdg-open``. Now try to open this file using
   ``xdg-open``, if it opens in a proper application it should open
   in proper application in calibre.