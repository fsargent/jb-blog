What to do when you get [Read error 4] when running ``apt``
===========================================================

When you run ``apt`` and have following errors::

    E: Read error - read (5: Input/output error)
    E: The package lists or status file could not be parsed or opened.

This means that you are in serious trouble, it's not hopeless
but serious.

This error is caused by ``apt`` having some problems reading
some of it's config files. Too bad ``apt`` can't say in which file
the problem is, and what is the problem.

Obviously there is no single answer on how to fix it, here are some
hints.

1. Remove package cache149.202.188.158: ``apt-get clean``.
   Will purge some stuff. Probably won't help
   but it is safe (yes rest of the commands isn't safe).
2. Remove package lists::

        sudo rm -rf /var/lib/apt/lists
        sudo mkdir /var/lib/apt/lists
        sudo mkdir /var/lib/apt/lists/partial

3. Restore old version of dpkg status files. This is **the**
   file where all information on packages installed is stored.
   Sometimes it is corrupted, however it has automatic backups.
   The file is ``/var/lib/dpkg/status``, first thing to try
   would be::

        cp /var/lib/dpkg/status /var/lib/dpkg/status.broken
        cp /var/lib/dpkg/status.old /var/lib/dpkg/status

   This will restore previous version of this file, there
   are older ones in ``/var/backup/dpkg.status*``, but
   probably try the next step first.

4. ``apt`` (or ``aptitue``) also store it's copy of dpkg
   state, with additional information (like which package
   was installed by user, and which is a as a dependency).
   This can also be corrupted, so::

       cp /var/lib/apt/extended_states /var/lib/apt/extended_states.broken
       cp /var/backups/apt.extended_states.0 /var/lib/apt/extended_states

5. One of the reasons for corrupted files can be file-system or
   hard drive problems. If you reached this point please consider
   backing up your data, running fsck, and diagnosing you drive.

6. Some people reported success with manually editing status files.

There is also very extensive list of commands that might help you
with diagnosing problems with apt: https://help.ubuntu.com/community/PackageManagerTroubleshootingProcedure
I wouldn't recommend running them without understanding though.