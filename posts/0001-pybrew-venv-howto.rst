
How to use pythonbrew and virtualenv
====================================


.. warning::

    This is outdated and not longer revelant.

In most of my projects I use virtualenvs and install dependencies by pip, well
it's nothing special, since virtualenvs have become mostly standard in python
developement.

But since among my colleagues I'm sort of early-adopter, and I find myself
explaining this again and again, I figured that I'll write this article.


What is a python virtual environment
------------------------------------

Virtual environment is *a way to manage python interpreter and associated
installed packages*. Generaly you use separate virtualenvs for all your
projects, in which case all these projects can depend on different versions
of libraries.

Advantages of using virtualenv
------------------------------

* Projects using different virtualenvs can use different versions of libraries.
* To install a library inside virtualenv you don't need to have root privelages,
  which is good even if you are root, because packages installed via `pip`
  shouldn't be trusted.

Installing virtualenv
---------------------

Normally you install virtualenv using your system package manager.

Using virtualenv
----------------

.. code-block:: bash

    virtualenv foo # create virtualenv in foo subfolder of local folder
    New python executable in foo/bin/python
    Installing distribute (..) done.
    Installing pip...............done.

    source foo/bin/activate # Activate the virtualenv

    pip install django #install packages
    Downloading/unpacking django
      Running setup.py egg_info for package django

        warning: no previously-included files matching '__pycache__' found under directory '*'
        warning: no previously-included files matching '*.py[co]' found under directory '*'
    Installing collected packages: django
      Running setup.py install for django
        changing mode of build/scripts-2.7/django-admin.py from 644 to 755

        warning: no previously-included files matching '__pycache__' found under directory '*'
        warning: no previously-included files matching '*.py[co]' found under directory '*'
        changing mode of /tmp/foo/bin/django-admin.py to 755
    Successfully installed django
    Cleaning up...

    python -c 'import django' #Verify django is installed

    deactivate # turn off the virtualenv

    python -c 'import django' # See that django was installed locally only!
    Traceback (most recent call last):
      File "<string>", line 1, in <module>
    ImportError: No module named django

Used commands

virtualenv path
    creates virtualenv in specified `path`. Interesting options include:

    * `virtualenv --python=python2.5` creates virtualenv using specified
      interpreter
    * `virtuaelnv --system-site-packages` allows virtualenv to use packages
      installed systemwide (it is nice since some packages can't be installed
      easily via pip).

source path/bin/activate
    Enables virtualenv in current console

pip install packagename
    installs package inside virtualenv (if it was activated)

deactivate
    disables virtualenv in current console

Pip cache trick
---------------

When using virtualenvs you'll find that installing packages via pip can be
painfully slow. You can instruct pip to use cache for downloaded packages
by adding following line to your `.bashrc` file.

.. code-block:: bash

    export PIP_DOWNLOAD_CACHE=$HOME/.pipcache

Using pythonbrew
----------------

Virtual enviorment *borrows* one of your system interpreters, pythonbrew takes
it step further: it downloads and compiles your own python interpreter (and
has integrated support for virtualenvs.

Installing pythonbrew
---------------------

Dont use instructions from pythonbrew repository, as they basically tell you:
'download file via http and execute it in bash` (which is *very insecure*).

Just clone repository stored in github at https://github.com/utahta/pythonbrew
and execute `install_pythonbrew.py` script.

Then as instructed paste following into bashrc file:

.. code-block:: bash

    [[ -s "$HOME/.pythonbrew/etc/bashrc" ]] && source "$HOME/.pythonbrew/etc/bashrc"


Using pythonbrew
----------------

pythonbrew install 2.7.1
    Installs python `2.7.1` into pythonbrew
pythonbrew use 2.7.1
    Use installed python `2.7.1` in current console.
pythonbrew venv create name -p 2.7.1
    Create virtualenv named `name` for python interpreter version `2.7.1`
pythonbrew venv use name -p 2.7.1
    Activate virtualenv named `name` for python interpreter version `2.7.1`

