Problems with nfs shares on Debian (in case of Vagrant)
=======================================================

If you are having problems with running Vagrant box with NFS shares,
here are possible solutions.

To install nfs you should need only to: ``apt-get install portmap nfs-kernel-server``.

If vagrant whines with::

  It appears your machine doesn't support NFS, or there is not an
  adapter to enable NFS on this machine for Vagrant. Please verify
  that `nfsd` is installed on your machine, and try again. If you're
  on Windows, NFS isn't supported. If the problem persists, please
  contact Vagrant support.

* Check if there is ``nfsd`` entry in ``/proc/filesystems`` if there is no
  such entry, or there is  only ``nfs`` or ``nfs4`` entry something is wrong
  with your host.
* Maybe ``nfs`` kernel module is not loaded? Try ``modprobe nfs; modprobe nfsd``
  and see if it helps,
* Maybe you forgot to install rules for ``nfs`` to your firewall?

  Check if you can telnet to:

  * Port 111 on your machine from your machint
  * Port 2049 on your machine from your machint
  * Port 111 on your machine from your vagrant box
  * Port 2049 on your machine from your vagrant box

* If you disabled ``ufw`` it might still screw your network communication,
  so::

    ufw enable
    ufw default allow
    ufw disable

  In my case after this and after restarting ``nfs-kernel-server`` everything
  worked like a charm.

* On debian I had a weird error in which nfs didn't start, because there were
  no exports defined, which in turn caused Vagrant to bail out with
  ``"It appears your machine doesn't support NFS... "``. This can be diagnosed
  if::

    root@karmapachyenno:~# systemctl status nfs-kernel-server.service
    ● nfs-kernel-server.service - LSB: Kernel NFS server support
       Loaded: loaded (/etc/init.d/nfs-kernel-server; bad; vendor preset: enabled)
       Active: active (exited) since Tue 2015-12-29 17:13:42 CET; 33min ago
         Docs: man:systemd-sysv-generator(8)
      Process: 949 ExecStart=/etc/init.d/nfs-kernel-server start (code=exited, status=0/SUCCESS)

    Dec 29 17:13:42 karmapachyenno systemd[1]: Starting LSB: Kernel NFS server support...
    Dec 29 17:13:42 karmapachyenno nfs-kernel-server[949]: Not starting NFS kernel daemon: no exports. ... (warning).
    Dec 29 17:13:42 karmapachyenno systemd[1]: Started LSB: Kernel NFS server support.

  This:

  * Can be fixed by adding any exports to ``/etc/exports``.
  * I fixed it by ``modprobe nfs; modprobe nfsd``, then running vagrant, which
    will add ``/etc/exports``, then reloading ``kernel-server`` and restarting
    vagrant.



